﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TrackPad_Teleport : MonoBehaviour {

    public GameObject CameraRig;
    public SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device device;
    private float AxisX = 0f;
    private float AxisY = 0f;
    
	// Update is called once per frame
	void Update () {
        device = SteamVR_Controller.Input((int)ArrowManager.Instance.trackedObj.index);
        AxisX = device.GetAxis().x * (device.GetPress(SteamVR_Controller.ButtonMask.Touchpad) ? 1 : 0);
        AxisY = device.GetAxis().y * (device.GetPress(SteamVR_Controller.ButtonMask.Touchpad) ? 1 : 0);
        if (Mathf.Abs(AxisX) > Mathf.Abs(AxisY))
        {
            if (AxisX > 0)
            {
                CameraRig.transform.position = new Vector3(-9f, 7.39f, 6.92f);
            }
            else if (AxisX < 0)
            {
                CameraRig.transform.position = new Vector3(0.11f, 7.39f, -12.6f);
            }
        }
        else
        {
            if (AxisY > 0)
            {
                CameraRig.transform.position = new Vector3(-14.28f, 7.39f, -7.46f);
            }
            else if (AxisY < 0)
            {
                CameraRig.transform.position = new Vector3(5.35f, 7.39f, 1.64f);
            }
        }
    }
}
