﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    private SteamVR_Controller.Device device;

    private bool isAttached = false;

    private bool isFired = false;

    private void OnTriggerStay()
    {
        AttachArrow();
    }

    void OnTriggerEnter()
    {
        AttachArrow();
    }

    void Update()
    {
        if (isFired && transform.GetComponent<Rigidbody>().velocity.magnitude > 5f)
        {
            transform.LookAt(transform.position + transform.GetComponent<Rigidbody>().velocity);
        }
    }

    public void Fired()
    {
        isFired = true;
    }

    private void AttachArrow()
    {
        device = SteamVR_Controller.Input((int)ArrowManager.Instance.trackedObj.index);

        if (device.GetPress(SteamVR_Controller.ButtonMask.Trigger))
        {
            ArrowManager.Instance.AttachBowToArrow();
            isAttached = true;
        }
    }
}
