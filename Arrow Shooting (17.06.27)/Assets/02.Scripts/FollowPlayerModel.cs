﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerModel : MonoBehaviour {

    Transform Player;

	// Use this for initialization
	void Start () {
        Player = GameObject.Find("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(Player.position.x, 0.0f, Player.position.z);
        //transform.eulerAngles = new Vector3(Player.eulerAngles.x, Player.eulerAngles.y, Player.eulerAngles.z);
	}
}
