﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavigationScript : MonoBehaviour
{

    public NavMeshAgent navMeshAgent;
    public Transform EnemyBody;
    

    public GameObject Destination;
    public GameObject SpawnPoint;
    GameObject[] SpawnPointArray;

    private int seconds = 10; //반복할 초

    // Use this for initialization
    void Start()
    {
        
        
        EnemyBody = GetComponent<Transform>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        SpawnPointArray = new GameObject[1];
        SpawnPointArray[0] = Instantiate(SpawnPoint) as GameObject;
        SpawnPointArray[0].SetActive(true);

        StartCoroutine(Spawn());


    }

    // Update is called once per frame
    void Update()
    {



       

    }


    IEnumerator Spawn()
    {

        while (true)
        {
            
            ChangeDestination();

            yield return new WaitForSeconds(Random.Range(10, 15)); //seconds마다 목적지가 바뀜
        }
    }

    void ChangeDestination()
    {


        float x = Random.Range(-20.0f, 20.0f);
        float y = Random.Range(-20.0f, 20.0f);
        SpawnPointArray[0].transform.position = new Vector3(x, 0.0f, y);

        navMeshAgent.SetDestination(Destination.transform.position);
        navMeshAgent.speed = Random.Range(1, 3);

        




    }
}




