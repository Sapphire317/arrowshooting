﻿using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour {

    public string mainMenu;

    public bool isPaused;

    public GameObject pauseMenuCanvas;

	// Use this for initialization
	void Start () {
        isPaused = false;
	}
	
	// Update is called once per frame
	void Update () {
		
        if(isPaused)
        {
            pauseMenuCanvas.SetActive(true);
            Time.timeScale = 0.0f;
        }
        else
        {
            pauseMenuCanvas.SetActive(false);
            Time.timeScale = 1.0f;
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;

        }
	}
    public void Click()
    {
        if (isPaused == false)
            isPaused = true;
        else if(isPaused == true)
            isPaused = false;

        Debug.Log(isPaused);
    }

    public void Resume()
    {
        isPaused = false;
    }
    public void Quit()
    {
        SceneManager.LoadScene("VJT Main Menu");
    }
}
