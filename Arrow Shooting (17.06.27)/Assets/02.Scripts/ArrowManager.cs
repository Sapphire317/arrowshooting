﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowManager : MonoBehaviour {

    public static ArrowManager Instance;

    public SteamVR_TrackedObject trackedObj;
    private SteamVR_Controller.Device device;

    private GameObject currentArrow;

    public GameObject stringAttachPoint;
    public GameObject arrowStartPoint;
    public GameObject stringStartPoint;

    public GameObject arrowPrefab;

    public GameObject Bow;

    private bool isAttached = false;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void OnDestroy()
    {
        if (Instance == this)
            Instance = null;
    }
	


	// Update is called once per frame
	void Update () {
        AttachArrow();
        PullString();
	}

    public void AttachArrow()
    {
        if (currentArrow == null)
        {
            // Create copy of arrow
            currentArrow = Instantiate(arrowPrefab);
            currentArrow.transform.parent = trackedObj.transform;
            currentArrow.transform.localPosition = new Vector3(0f, -0.13f, 0.15f);
            currentArrow.transform.localRotation = Quaternion.Euler(45f, 0f, 0f); //new Vector3(45f, 0f, 0f);
            currentArrow.SetActive(true);
        }
    }

    public void AttachBowToArrow()
    {
        currentArrow.transform.parent = stringAttachPoint.transform;
        currentArrow.transform.localPosition = arrowStartPoint.transform.localPosition;
        currentArrow.transform.rotation = arrowStartPoint.transform.rotation;

        isAttached = true;
    }

    private void PullString()
    {
        if (isAttached)
        {
            float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;
            stringAttachPoint.transform.localPosition = stringStartPoint.transform.localPosition + new Vector3 (5f*dist, 0f, 0f);
        }

        device = SteamVR_Controller.Input((int)ArrowManager.Instance.trackedObj.index);
        if (device.GetPressUp(SteamVR_Controller.ButtonMask.Trigger))
        {
            Fire();
        }
    }

    private void Fire()
    {
        float dist = (stringStartPoint.transform.position - trackedObj.transform.position).magnitude;

        currentArrow.transform.parent = null;
        currentArrow.GetComponent<Arrow>().Fired();


        Rigidbody r = currentArrow.GetComponent<Rigidbody>();
        //r.velocity = currentArrow.transform.forward * 20f * dist;
        r.AddForce(currentArrow.transform.forward * 20f* dist, ForceMode.VelocityChange);
        //currentArrow.transform.rotation = Bow.transform.rotation;
        //r.velocity = Bow.transform.forward * 20f * dist;
        r.useGravity = true;

        currentArrow.GetComponent<Collider>().isTrigger = true;

        stringAttachPoint.transform.localPosition = new Vector3 (1.416f, 0f, 0f); 
        currentArrow = null;
        isAttached = false;
    }

}

