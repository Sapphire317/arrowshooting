﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMgr : MonoBehaviour
{

    //AI 출현 위치담는 배열
    GameObject[] AIPool = null;
    

    int poolSize = 1;
    //AI Prefab 할당할 변수
    public GameObject AIPrefab;
    private Transform trans;


    //게임 종료 여부 변수a
    public bool isGameOver = false;

    // Use this for initialization
    void Start()

        
    {
        trans = GetComponent<Transform>();

        AIPool = new GameObject[poolSize];

        for (int i = 0; i < poolSize; ++i)
        {
            AIPool[i] = Instantiate(AIPrefab) as GameObject;
            AIPool[i].name = "AI_" + i;
            AIPool[i].SetActive(false);
        }
    }


    // Update is called once per frame
    void Update()
    {

        for (int i = 0; i < poolSize; ++i)
        {
            GameObject AIObj = AIPool[i];
            if (AIObj.activeSelf == true)
                continue;

            AIObj.SetActive(true);

            float x = Random.Range(-20.0f, 20.0f);
            float y = Random.Range(-20.0f, 20.0f);
            AIObj.transform.position = trans.position;
            break;
        }
    }
}
